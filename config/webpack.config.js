// const webpack = require("webpack");
const path = require("path");
const fs = require("fs");

const createBuildTemplate = require("template");

const appDirectory = fs.realpathSync(process.cwd());
const resolveAppPath = (relativePath) =>
  path.resolve(appDirectory, relativePath);

module.exports = createBuildTemplate(resolveAppPath, {
  resolve: {
    fallback: {
      os: false,
      crypto: false,
    },
  },
  module: {
    rules: [
      {
        test: /\.ya?ml$/,
        use: "yaml-loader",
      },
      {
        test: /\.corrupted/,
        use: "null-loader",
      },
      {
        test: /\.(jpg|png|webp)$/,
        use: "file-loader",
      },
    ],
  },
});
