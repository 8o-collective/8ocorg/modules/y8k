import styled from "styled-components";

const HeaderContainer = styled.div`
  margin: 0px;
  position: absolute;
  width: 100%;
  height: 4vw;
  top: 0px;
  right: 0px;

  font-family: "Courier Pixelated 16";
`;

const HeaderLinkContainer = styled.div`
  margin: 0px;
  position: absolute;
  height: 100%;
  top: 0px;
  right: 0px;

  display: flex;
  justify-content: space-evenly;
  align-items: center;
`;

const HeaderLink = styled.a`
  display: block;
  color: red;
  padding: 2vw;

  &:after {
    content: "";
    background: red;
    position: absolute;
    bottom: 25%;
    margin-left: calc(2vw - 1px);
    height: 50%;
    width: 1px;
    overflow-y: hidden;
  }
`;

const HeaderLogo = styled.img`
  position: absolute;
  height: 300%;
  top: 0px;
  left: 0px;
  padding: 2vh 3vw;

  cursor: pointer;
`;

export { HeaderContainer, HeaderLinkContainer, HeaderLink, HeaderLogo };
