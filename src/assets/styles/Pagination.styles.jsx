import styled from "styled-components";

const PaginationContainer = styled.div`
  position: relative;
  height: 5vh;
  width: 100%;

  font-family: "Courier Pixelated 16";
`;

const PaginationElementsContainer = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 20%;

  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
`;

const PaginationInput = styled.input`
  text-align: center;

  font-family: inherit;
  font-size: 1vw;
  color: red;
  background-color: black;

  border: solid red 1px;
  outline: none;

  width: 10%;
`;

const PaginationArrow = styled.div`
  font-size: 1vw;
  cursor: pointer;
  color: white;
  user-select: none;
  &:hover {
    color: red;
  }

  ${(props) => !props.active && "visibility: hidden"}
`;

export {
  PaginationContainer,
  PaginationElementsContainer,
  PaginationInput,
  PaginationArrow,
};
