import styled from "styled-components";

const PostContentContainer = styled.div`
  position: absolute;
  top: 10vh;
  left: 50%;
  transform: translate(-50%, 0);
  width: 85vw;

  font-family: IBM3270;

  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
`;

const PostTitle = styled.div`
  margin: 1vw;
  width: 100%;

  font-size: 3vw;
  color: white;

  font-family: "Times Pixelated 32";

  text-shadow: 1px 1px red;
`;

const PostRepliesContainer = styled.div`
  background-color: black;

  position: relative;
  width: 100%;

  border: solid red 1px;

  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
`;

const PostReplyContainer = styled.div`
  position: relative;
  width: 100%;

  border-bottom: solid red 1px;

  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;
`;

const PostReplyHeaderContainer = styled.div`
  position: relative;
  height: 100%;
  width: 15%;

  left: 0px;

  border-right: dashed red 1px;

  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;

  font-size: 0.8vw;

  cursor: pointer;

  padding: 1vw;

  color: white;

  &:hover > div {
    color: red;
    text-decoration: underline;
  }

  &:hover > span {
    color: red;
  }
`;

const PostReplyHeaderAvatar = styled.span`
  display: block;

  white-space: pre;

  color: white;
  text-decoration: none;

  font-size: inherit;

  // font-family: "Courier Pixelated 16";
  font-family: IBM3270;
`;

const PostReplyHeaderAuthor = styled.div`
  // margin: 1vw;

  color: inherit;
  text-decoration: inherit;

  padding: 2vh 0vw;

  font-size: 1vw;

  font-family: "Arial Pixelated 16";
`;

const PostReplyHeaderTime = styled.div`
  // margin: 1vw;

  color: white;
  text-decoration: none;

  font-size: inherit;

  font-family: "Courier Pixelated 16";
`;

const PostReplyContent = styled.div`
  position: relative;

  padding: 1vw;
  min-height: 2vw;
  width: 80%;

  color: white;

  font-family: "Times Pixelated 16";

  a {
    color: red !important;
  }

  blockquote {
    padding: 0.01% 1%;
    border-left: solid 1px red;
  }

  code {
    font-family: "IBM3270";
    border: solid 1px red;
    padding: 0.01% 1%;
  }
`;

const PostBackContainer = styled.div`
  padding: 1%;
  width: 95%;
`;

const PostBackLink = styled.a`
  color: red;

  font-family: "Courier Pixelated 16";
`;

export {
  PostContentContainer,
  PostTitle,
  PostRepliesContainer,
  PostReplyContainer,
  PostReplyHeaderContainer,
  PostReplyHeaderAvatar,
  PostReplyHeaderAuthor,
  PostReplyHeaderTime,
  PostReplyContent,
  PostBackContainer,
  PostBackLink,
};
