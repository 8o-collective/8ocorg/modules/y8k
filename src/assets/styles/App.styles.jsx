import styled from "styled-components";

import background from "assets/images/background.jpg";

const AppContainer = styled.div`
  background-image: url("${background}");
  background-repeat: repeat;
  // background-size: 30%;

  margin: 0px;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
`;

const AppHeaderContainer = styled.div`
  margin: 0px;
  position: sticky;
  width: 100%;
  top: 0px;
  left: 0px;

  display: flex;
  justify-content: space-evenly;
  border-right: solid red 1px;
  box-sizing: border-box;
`;

const AppHeaderElementContainer = styled.div`
  width: 100%;
  padding: 0.5vw;
  background-color: black;

  border: solid red 1px;
  border-right: none;
`;

export { AppContainer, AppHeaderContainer, AppHeaderElementContainer };
