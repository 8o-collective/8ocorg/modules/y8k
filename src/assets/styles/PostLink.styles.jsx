import styled from "styled-components";

const PostLinkContainer = styled.div`
  position: relative;
  height: 15vh;
  width: 100%;

  border-bottom: solid red 1px;
  cursor: pointer;

  color: white;
  text-shadow: 1px 1px red;

  &:hover {
    text-decoration: underline;
    color: red;

    text-shadow: 1px 1px white;
  }
`;

const PostLinkTitle = styled.div`
  position: absolute;
  top: 10%;
  left: 2%;
  width: 95%;

  font-size: 2vw;
  text-decoration: inherit;
  color: inherit;

  font-family: "Times Pixelated 24";
  text-shadow: inherit;
`;

const PostLinkAuthor = styled.div`
  position: absolute;
  bottom: 10%;
  left: 2%;

  font-size: 1vw;
  text-decoration: inherit;
  color: inherit;

  font-family: "Arial Pixelated 16";
`;

const PostLinkTime = styled.div`
  position: absolute;
  bottom: 10%;
  right: 2%;

  font-size: 1vw;
  text-decoration: inherit;
  color: inherit;

  font-family: "Arial Pixelated 16";
`;

export { PostLinkContainer, PostLinkTitle, PostLinkAuthor, PostLinkTime };
