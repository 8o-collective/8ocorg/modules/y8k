import styled from "styled-components";

const PostsContainer = styled.div`
  background-color: black;

  position: absolute;
  top: 10vh;
  left: 45%;
  transform: translate(-50%, 0);
  width: 50vw;

  border: solid red 1px;

  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
`;

export { PostsContainer };
