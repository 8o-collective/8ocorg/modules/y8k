export default {
  headers: {
    index: "/",
    rules: "/rules",
    mods: "/mods",
    login: "redlog",
  },
};
