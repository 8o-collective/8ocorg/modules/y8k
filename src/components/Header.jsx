import React from "react";

import {
  HeaderContainer,
  HeaderLinkContainer,
  HeaderLink,
  HeaderLogo,
} from "assets/styles/Header.styles.jsx";

import logo from "assets/images/logo.png";

import constants from "assets/constants.js";

const Header = () => (
  <HeaderContainer>
    <HeaderLogo src={logo} />
    <HeaderLinkContainer>
      {Object.entries(constants.headers).map(([header, path]) => (
        <HeaderLink
          href={
            path.startsWith("/") ? path : `https://${path}.${window.domain}`
          }
          key={header}
        >
          {location.href.endsWith(path) ? <b>{header}</b> : header}
        </HeaderLink>
      ))}
    </HeaderLinkContainer>
  </HeaderContainer>
);

export default Header;
