import React, { useState, useEffect } from "react";

import { AppContainer } from "assets/styles/App.styles.jsx";

import Header from "components/Header.jsx";
import Posts from "components/Posts.jsx";

const App = () => {
  const [height, setHeight] = useState(document.documentElement.scrollHeight);

  useEffect(() => {
    setHeight(document.documentElement.scrollHeight);
  }, []);

  return (
    <AppContainer style={{ height: `${height}px` }}>
      <Header />
      <Posts />
    </AppContainer>
  );
};

export default App;
