import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import ReactMarkdown from "react-markdown";

import { AppContainer as PostContainer } from "assets/styles/App.styles.jsx";

import {
  PostContentContainer,
  PostTitle,
  PostRepliesContainer,
  PostReplyContainer,
  PostReplyHeaderContainer,
  PostReplyHeaderAvatar,
  PostReplyHeaderAuthor,
  PostReplyHeaderTime,
  PostReplyContent,
  PostBackContainer,
  PostBackLink,
} from "assets/styles/Post.styles.jsx";

import { getPost } from "actions/posts.js";

const Post = () => {
  let { id } = useParams();
  const post = getPost(id);

  const [isRenderableTimeReadable, setIsRenderableTimeReadable] = useState(
    post.renderables.map(() => false)
  );

  const toggleReadableTime = (renderableIndex) =>
    setIsRenderableTimeReadable((prevReadables) => [
      ...prevReadables.slice(0, renderableIndex),
      !prevReadables[renderableIndex],
      ...prevReadables.slice(renderableIndex + 1),
    ]);

  const [height, setHeight] = useState(document.documentElement.scrollHeight);

  useEffect(() => {
    setHeight(document.documentElement.scrollHeight);
  }, []);

  return (
    <PostContainer style={{ height: `${height}px` }}>
      <PostContentContainer>
        <PostTitle>{post.title}</PostTitle>
        <PostRepliesContainer>
          {post.renderables.map((reply, i) => (
            <PostReplyContainer key={i}>
              <PostReplyHeaderContainer
                onClick={() =>
                  (window.location.href = reply.corrupted
                    ? `${window.location.protocol}//invite.${window.domain}`
                    : `${window.location.protocol}//registry.${window.domain}/${reply.author}`)
                }
                onMouseEnter={() => toggleReadableTime(i)}
                onMouseLeave={() => toggleReadableTime(i)}
              >
                {reply.avatar && (
                  <PostReplyHeaderAvatar>{reply.avatar}</PostReplyHeaderAvatar>
                )}
                {reply.avatar && console.log(reply.avatar)}
                <PostReplyHeaderAuthor>{reply.author}</PostReplyHeaderAuthor>
                <PostReplyHeaderTime>
                  {isRenderableTimeReadable[i]
                    ? reply.readableTime
                    : reply.time}
                </PostReplyHeaderTime>
              </PostReplyHeaderContainer>
              <PostReplyContent>
                <ReactMarkdown>{reply.content}</ReactMarkdown>
              </PostReplyContent>
            </PostReplyContainer>
          ))}
          <PostBackContainer>
            <PostBackLink href={document.referrer}>{"<"} Back</PostBackLink>
          </PostBackContainer>
        </PostRepliesContainer>
      </PostContentContainer>
    </PostContainer>
  );
};

export default Post;
