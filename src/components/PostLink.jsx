import React from "react";

import {
  PostLinkContainer,
  PostLinkTitle,
  PostLinkAuthor,
  PostLinkTime,
} from "assets/styles/PostLink.styles.jsx";

const PostLink = ({ post }) => (
  <PostLinkContainer
    onClick={() =>
      (window.location.href = post.log
        ? `${window.location.protocol}//redlog.${
            window.domain
          }/${encodeURIComponent(
            `log/usercomm/${Math.floor(post.time.getTime() / 1000)}`
          )}`
        : post.corrupted
        ? `${window.location.protocol}//invite.${window.domain}`
        : Math.floor(post.time.getTime() / 1000))
    }
  >
    <PostLinkTitle>{post.title}</PostLinkTitle>
    <PostLinkAuthor>{post.author}</PostLinkAuthor>
    <PostLinkTime>
      {post.corrupted ? post.corruptedTime : post.time.toUTCString()}
    </PostLinkTime>
  </PostLinkContainer>
);

export default PostLink;
