import React from "react";
import { useSearchParams } from "react-router-dom";

// import ReactMarkdown from "react-markdown";

import { PostsContainer } from "assets/styles/Posts.styles.jsx";

import Pagination from "components/Pagination.jsx";
import PostLink from "components/PostLink.jsx";

import { getPosts, maxPages } from "actions/posts.js";

const Posts = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const page = parseInt(searchParams.get("page")) || 0;
  const setPage = (v) => setSearchParams({ page: v });

  return (
    <PostsContainer>
      {getPosts(page).map((post) => (
        <PostLink post={post} key={post.time} />
      ))}
      <Pagination page={page} setPage={setPage} maxPage={maxPages} />
    </PostsContainer>
  );
};

export default Posts;
