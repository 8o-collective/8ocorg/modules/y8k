import parsePost, { createCorruptPost } from "./post/parsePost";
import logs from "./post/logs";

const extensionPattern = /\.[^/.]+$/;
const pathPattern = /^.*[\\/]/;
const getFilename = (full) =>
  full.replace(pathPattern, "").replace(extensionPattern, "");
const getExtension = (full) => full.match(extensionPattern)[0];

const resultsPerPage = 10;

const context = require.context(
  "./../assets/posts/",
  false,
  /\.(ya?ml$|corrupted)/
);
const relevant = context.keys().filter((e) => e.startsWith("."));
const content = relevant.map(context);
const posts = content
  .map((e, i) => ({
    ...e.default,
    corrupted: getExtension(relevant[i]) === ".corrupted",
    time: new Date(getFilename(relevant[i]) * 1000),
    ...(getExtension(relevant[i]) === ".corrupted" &&
      createCorruptPost(getFilename(relevant[i]) * 1000)),
  }))
  .concat(logs)
  .sort((a, b) => (a.time > b.time ? 1 : -1)); // chrome requires this weird ternary

const getPosts = (page) =>
  posts.slice(page * resultsPerPage, (page + 1) * resultsPerPage);

const maxPages = Math.floor(posts.length / resultsPerPage);

const getPost = (id) =>
  parsePost({
    ...require(`./../assets/posts/${id}.yaml`).default,
    time: new Date(id * 1000),
  });

export { getPosts, getPost, maxPages };
