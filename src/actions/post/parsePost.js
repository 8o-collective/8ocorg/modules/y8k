import randy from "randy";

import { generateGrid, generateChars, printGrid } from "./avatar";

const multipliers = [86400, 3600, 60, 1]; // day, hour, minute, second
const acronyms = ["d", "h", "m", "s"];
const corrupted = /<CORRUPTED ([0-9]+)>/g;

const corruptWithRandy = (length) =>
  Array.from({ length }, () =>
    String.fromCharCode(Math.floor(randy.randInt() % 8000))
  ).join("");

const createCorruptPost = (time) => {
  randy.setState({ seed: Array.from(Array(32)).map(() => time), idx: 1 });

  const corruptedString = (min, max) =>
    corruptWithRandy(Math.floor(randy.random() * (max - min) + min));

  return {
    title: corruptedString(10, 25),
    author: corruptedString(5, 20),
    corrupted: true,
    corruptedTime: corruptedString(14, 14),
  };
};

const createRandomTime = (replyIndex, replyArray) => {
  let prevTime = 0;
  for (let prevIndex = 0; prevIndex < replyIndex; prevIndex++) {
    if (replyArray[prevIndex].time) {
      prevTime = replyArray[prevIndex].time
        .split(";")
        .reduce((r, a, i) => r + a * multipliers[i], 0);
    }
  }

  let nextTime = 0;
  let nextIndex = replyIndex + 1;
  while (!nextTime) {
    if (!replyArray[nextIndex]) {
      break;
    }

    if (replyArray[nextIndex].time) {
      nextTime = replyArray[nextIndex].time
        .split(";")
        .reduce((r, a, i) => r + a * multipliers[i], 0);
    }
    nextIndex++;
  }

  let curTime = Math.floor(randy.random() * (nextTime - prevTime) + prevTime);

  return curTime;
};

const parsePost = (post) => {
  let seed = post.time;

  randy.setState({ seed: Array.from(Array(32)).map(() => seed), idx: 1 });

  const corruptedString = (min, max) =>
    corruptWithRandy(Math.floor(randy.random() * (max - min) + min));

  if (!post.replies.slice(-1)[0].time) {
    let lastReplyWithTime = post.replies.findLast((reply) => reply.time);
    if (lastReplyWithTime) {
      post.replies.slice(-1)[0].time = lastReplyWithTime.time;
    } else {
      post.replies.slice(-1)[0].time = "00;00;00;00";
    }
  }

  const replyTimeDeltaArray = post.replies
    .map((reply, index, replyArray) => {
      if (reply.corrupted) {
        return;
      }

      if (!reply.time) {
        return createRandomTime(index, replyArray);
      }

      return reply.time
        .split(";")
        .reduce((r, a, i) => r + a * multipliers[i], 0);
    })
    .sort(function (a, b) {
      return a - b;
    });

  for (let reply of post.replies) {
    if (reply.corrupted) {
      reply.deltaSeconds = 0;
    } else {
      reply.deltaSeconds = replyTimeDeltaArray.shift();
    }
  }

  post.replies = post.replies
    .map((reply) => {
      if (reply.corrupted) {
        return Array.from({ length: reply.corrupted }, () => ({
          author: corruptedString(5, 20),
          time: corruptedString(14, 14),
          readableTime: corruptedString(4, 10),
          content: corruptedString(10, 100),
          corrupted: true,
        }));
      }

      let time = new Date(post.time.getTime() + reply.deltaSeconds * 1000);
      let readableTime = (
        String(Math.floor(reply.deltaSeconds / multipliers[0])).padStart(
          2,
          "0"
        ) + new Date(reply.deltaSeconds * 1000).toJSON().slice(10, -5)
      )
        .split(/[:T]/g)
        .map((e, i) => e + acronyms[i])
        .join(" ");

      return {
        ...reply,
        avatar: printGrid(
          generateGrid(reply.author, 16, 8),
          generateChars(reply.author)
        ),
        time: time.toUTCString(),
        readableTime,
      };
    })
    .flat();

  post.renderables = [
    {
      author: post.author,
      avatar: printGrid(
        generateGrid(post.author, 16, 8),
        generateChars(post.author)
      ),
      content: post.content,
      time: post.time.toUTCString(),
      readableTime: post.time.toUTCString(),
    },
    ...post.replies,
  ];

  post.renderables.forEach((renderable) => {
    const matches = [...renderable.content.matchAll(corrupted)];
    matches.forEach(
      (match) =>
        (renderable.content = renderable.content.replace(
          match[0],
          corruptedString(match[1], match[1])
        ))
    );
  });

  return {
    title: post.title,
    time: post.time,
    renderables: post.renderables,
  };
};

export { createCorruptPost };

export default parsePost;
