const createGrid = (width, height, fill = 0) =>
  Array(height)
    .fill(0)
    .map(() => Array(width).fill(fill));

const updateCoordinates = ([x, y], direction, width, height) => {
  const moves = { "00": [1, 1], "01": [-1, 1], 10: [1, -1], 11: [-1, -1] };
  const [dx, dy] = moves[direction];
  return [(x + dx + width) % width, (y + dy + height) % height];
};

const traverseGrid = (grid, seedBinary, startX, startY) => {
  let [x, y] = [startX, startY];
  const height = grid.length;
  const width = grid[0].length;
  for (let i = 0; i < seedBinary.length; i += 2) {
    const direction = seedBinary.substring(i, i + 2);
    [x, y] = updateCoordinates([x, y], direction, width, height);
    grid[y][x]++;
  }
  return grid;
};

const hashWord = (input) => {
  const FNV_PRIME = BigInt("0x01000193");
  const OFFSET_BASIS = BigInt("0x811C9DC5");
  const MOD = BigInt("0x100000000000000000000000000000000"); // 2**128 to avoid overflow

  let hash = OFFSET_BASIS;
  for (let i = 0; i < input.length; i++) {
    hash ^= BigInt(input.charCodeAt(i));
    hash *= FNV_PRIME;
    hash %= MOD;
  }

  return hash;
};

const hashToBinary = (hash) => hash.toString(2).padStart(128, "0");

const generateGrid = (word, width, height) => {
  const seedBinary = hashToBinary(hashWord(word));
  const grid = createGrid(width, height);

  console.log(`Seed: ${seedBinary}`);

  const halfWidth = Math.floor(width / 2);
  const halfHeight = Math.floor(height / 2);

  // const startX = (seed % halfWidth) + halfWidth / 2;
  // const startY = ((seed / halfWidth) | 0) % halfHeight + halfHeight / 2;

  return traverseGrid(grid, seedBinary, halfWidth, halfHeight);
};

const generateChars = (word) => {
  const seed = hashWord(word);

  const potentials = {
    0: [" "],
    1: [".", ",", "-", "_"],
    2: ["o", "O", "Q", "D", "C", "G", "6", "9"],
    3: ["S", "5", "2", "Z"],
    4: ["E", "3", "8", "B"],
  };

  // get one character from each potential and put into an array
  const chars = Object.keys(potentials).map((key) => {
    const index = Number(seed.toString()[key]);
    return potentials[key][index % potentials[key].length];
  });

  console.log(chars);

  return chars;
};

const printGrid = (grid, chars) => {
  // const chars = [" ", ".", "o", "S", "E"];
  return (
    "+" +
    "-".repeat(grid[0].length) +
    "+" +
    "\n" +
    grid
      .map(
        (row) =>
          "|" +
          row.map((cell) => chars[Math.min(cell, chars.length - 1)]).join("")
      )
      .join("|\n") +
    "|\n" +
    "+" +
    "-".repeat(grid[0].length) +
    "+" +
    "\n"
  );
};

const word = "00bigbootyboy";
const grid = generateGrid(word, 16, 8);
console.log(printGrid(grid, generateChars(word)));

export { generateGrid, generateChars, printGrid };
