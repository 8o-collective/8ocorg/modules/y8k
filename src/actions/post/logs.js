const START_DATE = new Date(2001, 7, 29);
const END_DATE = new Date(2008, 3, 30);
const LOG_INTERVAL = 28; // days

const DURATION = Math.abs(END_DATE - START_DATE);
const DURATION_DAYS = Math.ceil(DURATION / (1000 * 60 * 60 * 24));
const NUM_LOGS = Math.floor(DURATION_DAYS / LOG_INTERVAL);

Date.prototype.addDays = function (days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
};

const logDates = [...Array(NUM_LOGS).keys()].map((i) =>
  START_DATE.addDays(LOG_INTERVAL * i)
);

// console.log(logDates.map(time => Math.floor(time / 1000)).join(",")) // use this to make directories

const logs = logDates.map((date) => ({
  title: `==== usercomm ${date.toDateString()} ====`,
  time: date,
  author: "--------",
  log: true,
}));

export default logs;
